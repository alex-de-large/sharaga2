package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.TilePane;

import java.util.ArrayList;
import java.util.List;

public class Controller {

    private List<User> users;

    @FXML private ListView<User> userListView;
    @FXML private TilePane tilePane;

    public Controller() {
        users = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            users.add(new User("qwerty" + i, "1" + i));
        }
    }


    @FXML
    private void initialize() {
        userListView.setCellFactory(param -> UserListCell.newInstance(userEditor));
        userListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
//        userListView.setOrientation(Orientation.HORIZONTAL);
//        userListView.setOnMouseClicked(event -> {
//            System.out.println(userListView.getSelectionModel().getSelectedItem().getUsername());
//        });
        userListView.getItems().addAll(users);
    }

    private Editor<User> userEditor = user -> {
        TestDialog testDialog = new TestDialog(Main.getStage(),1);
        testDialog.show();
        System.out.println(user.getUsername());
        return user;
    };

    private void showMessage(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

    }
}
