package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class UserListCell extends ListCell<User> implements Initializable {

    @FXML private Label nameLabel;
    @FXML private Label phoneLabel;
    @FXML private AnchorPane root;
    @FXML private Button button;

    private User model;
    private Editor<User> userEditor;

    public static UserListCell newInstance(Editor<User> userEditor) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(Main.class.getResource("test.fxml"));

        try {
            fxmlLoader.load();
            UserListCell userListCell = fxmlLoader.getController();
            userListCell.setUserEditable(userEditor);
            return userListCell;
        } catch (IOException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    @Override
    protected void updateItem(User item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            nameLabel.setStyle("-fx-text-fill: #000000");
            nameLabel.setText(item.getUsername());
            phoneLabel.setText(item.getPhoneNumber());
            button.setOnAction(event -> {
                userEditor.edit(model);
            });
        }
        this.model = item;
        setGraphic(root);
    }

    private void setUserEditable(Editor<User> userEditor) {
        this.userEditor = userEditor;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setGraphic(root);
    }
}
