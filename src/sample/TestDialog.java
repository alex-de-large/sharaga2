package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Window;

import java.io.IOException;

public class TestDialog extends Dialog<Integer> {

    @FXML private TextField textField;

    public TestDialog(Window owner, int defaultValue) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(Main.class.getResource("dialog.fxml"));
        fxmlLoader.setController(this);

        DialogPane dialogPane = null;
        try {
            dialogPane = fxmlLoader.load();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        initOwner(owner);
        initModality(Modality.APPLICATION_MODAL);
        setDialogPane(dialogPane);

        textField.setText(Integer.toString(defaultValue));
    }

    @FXML
    private void onClick(ActionEvent actionEvent) {
        int value = 0;
        try {
            value = Integer.parseInt(textField.getText());
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            close();
        }
        setResult(value);
        close();
    }
}
