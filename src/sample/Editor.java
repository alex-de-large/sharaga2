package sample;

public interface Editor<T> {

    T edit(T t);
}
